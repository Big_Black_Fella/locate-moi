package moi.centric.com.locatemoi;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;

public class MapsActivity extends Activity implements LocationListener, OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;
    private LatLng selectedLocation;
    private LatLng defaultLocation;
    private LocationManager locationManager;
    Location userLocation;
    LocationListener listener;
    private String provider;
    private HashMap<CharSequence, LatLng> hmap = new HashMap<CharSequence, LatLng>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            userLocation = locationManager.getLastKnownLocation(provider);
            return;
        }

        // Initialize the location fields
        if (userLocation != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(userLocation);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Obtain the MapFragment and get notified when the map is ready to be used.
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        hmap.put("Ekiti", new LatLng(7.63, 5.2));
        hmap.put("Lagos", new LatLng(6.41, 3.45));
        hmap.put("Ogun", new LatLng(7.05, 3.32));
        hmap.put("Osun", new LatLng(7.5, 4.52));
        hmap.put("Ondo", new LatLng(7.07, 4.78));
        hmap.put("Oyo", new LatLng(7.77, 3.93));
        hmap.put("Abuja", new LatLng(9.27, 7.03));
        hmap.put("Nasarawa", new LatLng(8.53, 7.68));
        hmap.put("Kwara", new LatLng(8.5, 4.58));
        hmap.put("Jos", new LatLng(9.88, 8.85));
        hmap.put("Kogi", new LatLng(7.56, 6.58));
        hmap.put("Niger", new LatLng(10.22, 5.39));
        hmap.put("Benue", new LatLng(7.35, 8.84));
        hmap.put("Edo", new LatLng(6.33, 5.51));
        hmap.put("Delta", new LatLng(5.53, 5.89));
        hmap.put("Rivers", new LatLng(4.86, 6.92));
        hmap.put("Cross_River", new LatLng(6.17, 8.66));
        hmap.put("Bayelsa", new LatLng(4.87, 5.89));
        hmap.put("Bauchi", new LatLng(10.37, 9.8));
        hmap.put("Adamawa", new LatLng(9.17, 12.48));
        hmap.put("Gombe", new LatLng(10.32, 11.03));
        hmap.put("Yobe", new LatLng(12.32, 12.19));
        hmap.put("Taraba", new LatLng(8.65, 11.64));
        hmap.put("Borno", new LatLng(11.86, 13.64));
        hmap.put("Kaduna", new LatLng(10.50, 7.35));
        hmap.put("Kano", new LatLng(12.03, 8.5));
        hmap.put("Katsina", new LatLng(7.17, 9.03));
        hmap.put("Sokoto", new LatLng(13.03, 5.26));
        hmap.put("Kebbi", new LatLng(12.53, 4.2));
        hmap.put("Zamfara", new LatLng(12.31, 6.40));
        hmap.put("Jigawa", new LatLng(12.75, 10.20));
        hmap.put("Enugu", new LatLng(6.5, 7.5));
        hmap.put("Abia", new LatLng(5.55, 7.48));
        hmap.put("Anambra", new LatLng(6.47, 7.02));
        hmap.put("Ebonyi", new LatLng(6.31, 8.61));
        hmap.put("Imo", new LatLng(5.87, 7.53));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.setBuildingsEnabled(true);
            mMap.setMapType(1);
            mMap.setTrafficEnabled(true);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            return;
        }
        if (defaultLocation != null) {
            mMap.addMarker(new MarkerOptions().position(defaultLocation).title("Your Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(defaultLocation));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 1000, null);
        }else{
            mMap.addMarker(new MarkerOptions().position(new LatLng(6.41, 3.45)).title("Lagos"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(6.41, 3.45)));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 1000, null);
        }
    }


    public void resetMap(CharSequence location,GoogleMap googleMap) {
        selectedLocation = hmap.get(location);
        googleMap.addMarker(new MarkerOptions().position(selectedLocation).title(location.toString()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(selectedLocation));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(7), 1000, null);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        CharSequence title = item.getTitle();
//        selectedLocation = hmap.get(title);
//        mMap.addMarker(new MarkerOptions().position(selectedLocation).title(selectedLocation.toString()));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(selectedLocation));

        this.resetMap(title, mMap);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        defaultLocation = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(provider, 10, 1.0F, (android.location.LocationListener) listener);
            return;
        }
    }

    /*@Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }*/

   /* @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }*/
}
